﻿Imports System.IO
Imports Sudoku.Core

Module Module1

    Sub Main()
        Using fileReader = New StreamReader("sudoku17.txt")
            Using db = New sudokusprint_dbEntities
                fileReader.ReadLine() ' ignore first line.
                Dim line = fileReader.ReadLine
                Dim puzzle = New Sudoku.Core.Puzzle
                Do While line IsNot Nothing
                    puzzle.FromString(line)
                    puzzle.LockAllNonEmptySquares()
                    line = puzzle.ToString
                    Dim dbPuzzle = New Puzzle
                    Dim valParts = line.Split(":")
                    dbPuzzle.Values = valParts(0)
                    dbPuzzle.LockedValues = valParts(1)
                    dbPuzzle.IsSolved = False
                    db.Puzzles.Add(dbPuzzle)
                    db.SaveChanges()
                    Console.WriteLine(line)
                    line = fileReader.ReadLine
                Loop
                fileReader.Close()
            End Using
        End Using

        Console.WriteLine("Done")
        Console.ReadLine()

    End Sub

End Module
