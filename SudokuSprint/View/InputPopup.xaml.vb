﻿Imports SudokuSprint.ViewModel

Partial Public Class InputPopup
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub Button_Tap(sender As System.Object, e As Input.GestureEventArgs)
        If DataContext IsNot Nothing AndAlso TypeOf DataContext Is InputViewModel Then
            CType(DataContext, InputViewModel).InputVisible = False
        End If
        e.Handled = True
    End Sub

    Private Sub UserControl_Tap(sender As System.Object, e As Input.GestureEventArgs)
        e.Handled = True
    End Sub

    Private Sub UserControl_LostFocus(sender As System.Object, e As RoutedEventArgs)
        If DataContext IsNot Nothing AndAlso TypeOf DataContext Is InputViewModel Then
            CType(DataContext, InputViewModel).UpdatePuzzle()
        End If
    End Sub
End Class
