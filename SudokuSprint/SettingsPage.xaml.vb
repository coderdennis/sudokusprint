﻿Imports SudokuSprint.ViewModel

Partial Public Class SettingsPage
    Inherits PhoneApplicationPage

    Private Const ViewModelStateName As String = "ViewModelState"
    Private _isNewPageInstance As Boolean

    Public Sub New()
        InitializeComponent()
        _isNewPageInstance = True
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs)
        CType(LayoutRoot.DataContext, SettingsViewModel).Save()
        NavigationService.GoBack()
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As EventArgs)
        NavigationService.GoBack()
    End Sub

    Protected Overrides Sub OnNavigatedFrom(e As NavigationEventArgs)
        If e.NavigationMode <> NavigationMode.Back Then
            State(ViewModelStateName) = CType(LayoutRoot.DataContext, SettingsViewModel).State
        End If
    End Sub

    Protected Overrides Sub OnNavigatedTo(e As NavigationEventArgs)
        If _isNewPageInstance Then
            If State.Keys.Contains(ViewModelStateName) Then
                CType(LayoutRoot.DataContext, SettingsViewModel).State = State(ViewModelStateName)
            End If
        End If
        _isNewPageInstance = False
    End Sub

End Class