﻿Imports Sudoku.Core
Imports System.Windows.Media.Imaging

Namespace Helpers
    Public Class ValueImages

        Private Shared ReadOnly Images As Dictionary(Of Values, WriteableBitmap) = New Dictionary(Of Values, WriteableBitmap)
        Private Shared ReadOnly NotesImages As Dictionary(Of Values, WriteableBitmap) = New Dictionary(Of Values, WriteableBitmap)

        Public Shared Function Image(ByVal value As Values) As WriteableBitmap
            If Not Images.ContainsKey(value) Then
                Images.Add(value, New WriteableBitmap(0, 0).FromResource(String.Format("Images/value{0}.png", value.ToString("d"))))
            End If
            Return Images(value)
        End Function

        Public Shared Function NoteImage(ByVal value As Values) As WriteableBitmap
            If Not NotesImages.ContainsKey(value) Then
                NotesImages.Add(value, New WriteableBitmap(0, 0).FromResource(String.Format("Images/note{0}.png", value.ToString("d"))))
            End If
            Return NotesImages(value)
        End Function

    End Class
End Namespace