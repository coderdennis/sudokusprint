﻿Imports System.Runtime.CompilerServices

Namespace Helpers
    Public Module ViewExtensions

        <Extension()>
        Public Function AsVisibility(ByVal value As Boolean) As Visibility
            If value Then
                Return Visibility.Visible
            Else
                Return Visibility.Collapsed
            End If
        End Function

    End Module
End Namespace