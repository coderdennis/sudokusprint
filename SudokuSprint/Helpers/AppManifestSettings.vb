﻿Imports System.Xml.Linq

Namespace Helpers

    Public Class AppManifestSettings

        Private Shared Function GetWinPhoneAttribute(ByVal attributeName As String) As String
            Dim manifestXml = XDocument.Load("WMAppManifest.xml")
            Dim appElement = manifestXml.Descendants("App").SingleOrDefault()
            If appElement IsNot Nothing Then
                Return appElement.Attribute(attributeName).Value
            End If
            Return ""
        End Function

        Private Shared _applicationTitle As String

        Public Shared ReadOnly Property ApplicationTitle As String
            Get
                If _applicationTitle Is Nothing Then
                    _applicationTitle = GetWinPhoneAttribute("Title")
                End If
                Return _applicationTitle
            End Get
        End Property

    End Class

End Namespace
