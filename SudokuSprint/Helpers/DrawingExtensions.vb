﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Media.Imaging
Imports Sudoku.Core

Namespace Helpers
    Public Module DrawingExtensions

        <Extension()>
        Public Function ValueVisible(ByVal square As Square) As Boolean
            Return square.Value <> Values.Empty
        End Function

        <Extension()>
        Public Sub DrawSquare(ByVal bitmap As WriteableBitmap, ByVal square As Square, ByVal squarePoint As Point, ByVal squareSize As Integer, ByVal isSelected As Boolean, ByVal isHighlighted As Boolean)
            If isSelected Then
                bitmap.FillRectangle(squarePoint.X, squarePoint.Y, squarePoint.X + squareSize, squarePoint.Y + squareSize, ResourceHelper.GetColorFromResourceBrush("PhoneAccentBrush"))
            Else
                bitmap.FillRectangle(squarePoint.X, squarePoint.Y, squarePoint.X + squareSize, squarePoint.Y + squareSize, Colors.Transparent)
            End If
            If isHighlighted Then
                bitmap.DrawRectangle(squarePoint.X - 2, squarePoint.Y - 2, squarePoint.X + squareSize + 2, squarePoint.Y + squareSize + 2, ResourceHelper.GetColorFromResourceBrush("PhoneContrastBackgroundBrush"))
                bitmap.DrawRectangle(squarePoint.X - 1, squarePoint.Y - 1, squarePoint.X + squareSize + 1, squarePoint.Y + squareSize + 1, ResourceHelper.GetColorFromResourceBrush("PhoneContrastBackgroundBrush"))
                bitmap.DrawRectangle(squarePoint.X, squarePoint.Y, squarePoint.X + squareSize, squarePoint.Y + squareSize, ResourceHelper.GetColorFromResourceBrush("PhoneContrastBackgroundBrush"))
                bitmap.DrawRectangle(squarePoint.X + 1, squarePoint.Y + 1, squarePoint.X + squareSize - 1, squarePoint.Y + squareSize - 1, ResourceHelper.GetColorFromResourceBrush("PhoneContrastBackgroundBrush"))
            Else
                bitmap.DrawRectangle(squarePoint.X, squarePoint.Y, squarePoint.X + squareSize, squarePoint.Y + squareSize, ResourceHelper.GetColorFromResourceBrush("PhoneBorderBrush"))
            End If

            If square.ValueVisible Then
                Dim valueImage = ValueImages.Image(square.Value)
                Dim point = New Point(squarePoint.X + GetImagePosition(squareSize, valueImage.PixelWidth, NotePositions.Center),
                                      squarePoint.Y + GetImagePosition(squareSize, valueImage.PixelHeight, NotePositions.Center))
                Dim sourceRect = New Rect(0, 0, valueImage.PixelWidth, valueImage.PixelHeight)
                If square.Locked Then
                    bitmap.Blit(point, valueImage, sourceRect, ResourceHelper.GetColorFromResourceBrush("PhoneAccentBrush"), WriteableBitmapExtensions.BlendMode.Alpha)
                Else
                    bitmap.Blit(point, valueImage, sourceRect, ResourceHelper.GetColorFromResourceBrush("PhoneForegroundBrush"), WriteableBitmapExtensions.BlendMode.Alpha)
                End If
            Else
                For Each v In square.PossibleValues
                    ShowNote(bitmap, squarePoint.X, squarePoint.Y, squareSize, v)
                Next
            End If
        End Sub

        Private Sub ShowNote(ByVal bitmap As WriteableBitmap, ByVal x As Integer, ByVal y As Integer, ByVal squareSize As Integer, ByVal noteValue As Values)
            Dim noteImage = ValueImages.NoteImage(noteValue)
            Dim notePositionX As NotePositions
            Dim notePositionY As NotePositions
            Select Case noteValue
                Case Values.One
                    notePositionX = NotePositions.TopLeft
                    notePositionY = NotePositions.TopLeft
                Case Values.Two
                    notePositionX = NotePositions.Center
                    notePositionY = NotePositions.TopLeft
                Case Values.Three
                    notePositionX = NotePositions.BottomRight
                    notePositionY = NotePositions.TopLeft
                Case Values.Four
                    notePositionX = NotePositions.TopLeft
                    notePositionY = NotePositions.Center
                Case Values.Five
                    notePositionX = NotePositions.Center
                    notePositionY = NotePositions.Center
                Case Values.Six
                    notePositionX = NotePositions.BottomRight
                    notePositionY = NotePositions.Center
                Case Values.Seven
                    notePositionX = NotePositions.TopLeft
                    notePositionY = NotePositions.BottomRight
                Case Values.Eight
                    notePositionX = NotePositions.Center
                    notePositionY = NotePositions.BottomRight
                Case Values.Nine
                    notePositionX = NotePositions.BottomRight
                    notePositionY = NotePositions.BottomRight
            End Select
            Dim point = New Point(x + GetImagePosition(squareSize, noteImage.PixelWidth, notePositionX), y + GetImagePosition(squareSize, noteImage.PixelHeight, notePositionY))
            Dim sourceRect = New Rect(0, 0, noteImage.PixelWidth, noteImage.PixelHeight)
            bitmap.Blit(point, noteImage, sourceRect, ResourceHelper.GetColorFromResourceBrush("PhoneForegroundBrush"), WriteableBitmapExtensions.BlendMode.Alpha)
        End Sub

        Private Enum NotePositions
            TopLeft
            Center
            BottomRight
        End Enum

        Private Function GetImagePosition(ByVal totalSize As Double, ByVal imageSize As Double, ByVal position As NotePositions) As Double
            Select Case position
                Case NotePositions.TopLeft
                    Return (totalSize / 3 - imageSize) / 2
                Case NotePositions.Center
                    Return (totalSize - imageSize) / 2
                Case NotePositions.BottomRight
                    Return totalSize - GetImagePosition(totalSize, imageSize, NotePositions.TopLeft) - imageSize
                Case Else
                    Return 0
            End Select
        End Function

    End Module
End Namespace