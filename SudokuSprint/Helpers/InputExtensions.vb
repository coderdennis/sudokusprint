﻿Imports System.Runtime.CompilerServices

Namespace Helpers
    Module InputExtensions

        <Extension()>
        Public Function ButtonBackground(ByVal valueSelected As Boolean) As Brush
            If valueSelected Then
                Return Application.Current.Resources("PhoneAccentBrush")
            Else
                Return Application.Current.Resources("PhoneChromeBrush")
            End If
        End Function

    End Module
End Namespace