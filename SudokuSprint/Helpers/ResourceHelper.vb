﻿Namespace Helpers
    Public Class ResourceHelper

        Public Shared Function GetColorFromResourceBrush(ByVal resourceName As String) As Color
            Dim brush As SolidColorBrush = Application.Current.Resources(resourceName)
            Return brush.Color
        End Function

    End Class
End Namespace