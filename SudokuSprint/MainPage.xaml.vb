﻿Imports System.IO.IsolatedStorage
Imports System.IO
Imports PuzzleSource
Imports SudokuSprint.ViewModel

Partial Public Class MainPage
    Inherits PhoneApplicationPage

    Private ReadOnly _viewModel As MainViewModel
    Private Const FolderName As String = "SavedState"
    Private Const GameStateXmlFile As String = "\gameState.xml"
    Private _isNewPageInstance As Boolean
    Private ReadOnly _puzzleSource As IPuzzleSource

    ' Constructor
    Public Sub New()
        InitializeComponent()
        _puzzleSource = GetPuzzleSource()
        _viewModel = New MainViewModel(_puzzleSource)
        LayoutRoot.DataContext = _viewModel
        _isNewPageInstance = True
    End Sub

    'Private Sub SettingsButton_Click(sender As System.Object, e As System.EventArgs)
    '    NavigationService.Navigate(New Uri("/SettingsPage.xaml", UriKind.RelativeOrAbsolute))
    'End Sub

    Private Sub AboutButton_Click(sender As System.Object, e As EventArgs)
        NavigationService.Navigate(New Uri("/YourLastAboutDialog;component/AboutPage.xaml", UriKind.Relative))
    End Sub

    Private Sub MainPuzzleView_Tap(sender As System.Object, e As Input.GestureEventArgs)
        _viewModel.ShowInput(e.GetPosition(MainPuzzleView))
        e.Handled = True
    End Sub

    Private Sub ResetButton_Click(sender As System.Object, e As EventArgs)
        If MessageBox.Show("Reset this puzzle?", "Puzzle Reset", MessageBoxButton.OKCancel) = MessageBoxResult.OK Then
            _viewModel.ResetPuzzle()
        End If
    End Sub

    Private Sub NewPuzzleButton_Click(sender As System.Object, e As EventArgs)
        If MessageBox.Show("Start a new puzzle?", "New Puzzle", MessageBoxButton.OKCancel) = MessageBoxResult.OK Then
            _viewModel.NewPuzzle()
        End If
    End Sub

    Protected Overrides Sub OnNavigatedFrom(e As NavigationEventArgs)
        _viewModel.StopTimer()
        _puzzleSource.SaveState()
        Dim myStore = IsolatedStorageFile.GetUserStoreForApplication

        If Not myStore.DirectoryExists(FolderName) Then
            myStore.CreateDirectory(FolderName)
        End If
        Using isoFileStream = New IsolatedStorageFileStream(FolderName & GameStateXmlFile, FileMode.Create, myStore)
            Using isoFileWriter = New StreamWriter(isoFileStream)
                isoFileWriter.Write(_viewModel.State)
            End Using
        End Using
    End Sub

    Protected Overrides Sub OnNavigatedTo(e As NavigationEventArgs)
        If _isNewPageInstance Then
            Dim myStore = IsolatedStorageFile.GetUserStoreForApplication
            If myStore.FileExists(FolderName & GameStateXmlFile) Then
                Using isoFileStream = New IsolatedStorageFileStream(FolderName & GameStateXmlFile, FileMode.Open, myStore)
                    Using isoFileReader = New StreamReader(isoFileStream)
                        _viewModel.State = isoFileReader.ReadToEnd
                    End Using
                End Using
            Else
                _viewModel.NewPuzzle()
            End If
        End If
        _isNewPageInstance = False
        _viewModel.StartTimer()
    End Sub

    'Private Sub AdControl_AdRefreshed(ByVal sender as Object, ByVal e as System.EventArgs)
    '    Deployment.Current.Dispatcher.BeginInvoke(Sub()
    '                                                  AdDuplexAdControl.Visibility = Visibility.Collapsed
    '                                                  MSAdControl.Visibility = Visibility.Visible
    '                                              End Sub)
    'End Sub

    'Private Sub AdControl_ErrorOccurred(ByVal sender as Object, ByVal e as Microsoft.Advertising.AdErrorEventArgs)
    '    Deployment.Current.Dispatcher.BeginInvoke(Sub()
    '                                                  MSAdControl.Visibility = Visibility.Collapsed
    '                                                  AdDuplexAdControl.Visibility = Visibility.Visible
    '                                              End Sub)
    'End Sub

    Private Sub PhoneApplicationPage_Loaded(sender As Object, e As RoutedEventArgs)
        AdRotator.Invalidate()
    End Sub
End Class
