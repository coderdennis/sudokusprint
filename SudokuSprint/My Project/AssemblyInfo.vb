﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("Sudoku Sprint")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyConfiguration("")> 
<Assembly: AssemblyCompany("Dennis Palmer")> 
<Assembly: AssemblyProduct("Sudoku Sprint")> 
<Assembly: AssemblyCopyright("Copyright © Dennis Palmer 2013")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: AssemblyCulture("")> 

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components.  If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type.
<Assembly: ComVisible(False)> 

' The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c52e3945-c8a9-473b-8177-36d8ccddcf5c")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Revision and Build Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("1.2.0")> 
<Assembly: AssemblyFileVersion("1.2.0")> 

<Assembly: NeutralResourcesLanguageAttribute("en-US")> 