﻿Imports System.Windows.Media.Imaging
Imports SudokuSprint.Helpers
Imports Sudoku.Core
Imports PuzzleSource

Namespace ViewModel
    Public Class PuzzleViewModel
        Inherits ViewModelBase

        Private Const SquareSize As Integer = 50
        Private Const PuzzleSize As Integer = 451

        Private _puzzle As Puzzle
        Private ReadOnly _input As InputViewModel
        Private ReadOnly _bitmap As WriteableBitmap
        Private ReadOnly _squareLocations As List(Of Point)
        Private ReadOnly _rowOffsets As List(Of Integer)
        Private ReadOnly _colOffsets As List(Of Integer)
        Private _puzzleIsSolved As Boolean

        Private ReadOnly _puzzleSource As IPuzzleSource

        Public Sub New(ByVal puzzleSource As IPuzzleSource)
            Me.New(New Puzzle, puzzleSource)
        End Sub

        Public Sub New(ByVal puzzle As Puzzle, ByVal puzzleSource As IPuzzleSource)
            _puzzleSource = puzzleSource
            _puzzleIsSolved = False
            _input = New InputViewModel(Me)
            AddHandler _input.NotesChanged, AddressOf NotesChanged
            _puzzle = puzzle
            AddHandler _puzzle.SquareValueChanged, AddressOf SquareValueChanged
            _bitmap = New WriteableBitmap(PuzzleSize, PuzzleSize)
            _squareLocations = New List(Of Point)
            _rowOffsets = New List(Of Integer)
            _colOffsets = New List(Of Integer)
            For row = 0 To 8
                Dim x = GetOffset(row)
                _rowOffsets.Add(x)
                For col = 0 To 8
                    Dim y = GetOffset(col)
                    _colOffsets.Add(y)
                    _squareLocations.Add(New Point(x, y))
                Next
            Next
            UpdateImage()
        End Sub

        Public Property Puzzle As Puzzle
            Get
                Return _puzzle
            End Get
            Set(value As Puzzle)
                _puzzle = value
                AddHandler _puzzle.SquareValueChanged, AddressOf SquareValueChanged
                If _puzzle IsNot Nothing Then
                    UpdateImage()
                End If
            End Set
        End Property

        Private Function GetOffset(ByVal rowCol As Integer) As Integer
            Dim result = (rowCol * SquareSize)
            Return result
        End Function

        Friend Sub UpdateImage()
            If _puzzle IsNot Nothing Then
                For i = 0 To 80
                    _bitmap.DrawSquare(_puzzle.Squares(i), _squareLocations(i), SquareSize, False, False)
                Next
                If _input.InputVisible Then
                    Dim row = _puzzle.RowFromSquare(_input.Square)
                    Dim col = _puzzle.ColFromSquare(_input.Square)
                    For Each sq In row.Union(col)
                        _bitmap.DrawSquare(sq, _squareLocations(_puzzle.Squares.IndexOf(sq)), SquareSize, False, True)
                    Next
                    _bitmap.DrawSquare(_input.Square, _squareLocations(_puzzle.Squares.IndexOf(_input.Square)), SquareSize, True, True)
                End If
                OnPropertyChanged("PuzzleImage")
            End If
        End Sub

        Private Sub SquareValueChanged(ByVal square As Object, ByVal e As SquareValueChangedEventArgs)
            UpdateImage()
            If e.NewValue = Values.Empty Then
                _puzzleIsSolved = False
            ElseIf _puzzle.IsSolved Then
                _puzzleIsSolved = True
            End If
        End Sub

        Public ReadOnly Property IsSolved As Boolean
            Get
                Return _puzzleIsSolved
            End Get
        End Property

        Private Sub NotesChanged(ByVal sender As Object, ByVal e As EventArgs)
            UpdateImage()
        End Sub

        Public ReadOnly Property InputContext As InputViewModel
            Get
                Return _input
            End Get
        End Property

        Public ReadOnly Property PuzzleImage As ImageSource
            Get
                Return _bitmap
            End Get
        End Property

        Public Sub ShowInput(ByVal point As Point)
            If _puzzle IsNot Nothing Then
                If point.Y < 0 Then
                    point.Y = 0
                End If
                If point.X < 0 Then
                    point.X = 0
                End If
                Dim row = _rowOffsets.IndexOf(_rowOffsets.Where(Function(r) r <= point.Y).Last)
                Dim col = _colOffsets.IndexOf(_colOffsets.Where(Function(c) c <= point.X).Last)
                Dim square = _puzzle.Squares(col * 9 + row)
                If square.Locked Then
                    InputContext.InputVisible = False
                Else
                    If row < 4 Then
                        InputContext.Top = _rowOffsets(row + 1) + 1
                    Else
                        InputContext.Top = _rowOffsets(row) - 266 - 1
                    End If
                    If col < 5 Then
                        InputContext.Left = _colOffsets(col + 1) + 1
                    Else
                        InputContext.Left = _colOffsets(col) - 200 - 1
                    End If
                    InputContext.Square = square
                    InputContext.InputVisible = True
                End If
                UpdateImage()
            End If
        End Sub

        Public Sub ResetPuzzle()
            _puzzle.Reset()
        End Sub

        Public Sub NewPuzzle()
            Dim newPuzzle = _puzzleSource.GetPuzzle
            newPuzzle.SuspendSquareValueChangedEvent = True
            _puzzleIsSolved = False
            'Dim settings = New SettingsViewModel
            'For Each sq In puzzle.Squares.RandomSample(settings.Difficulty, False)
            '    sq.Locked = True
            'Next
            'For Each sq In puzzle.UnlockedSquares
            '    sq.Value = Values.Empty
            'Next
            newPuzzle.LockAllNonEmptySquares()

            newPuzzle.SuspendSquareValueChangedEvent = False
            newPuzzle.ResetAllPossibleValues()
            Puzzle = newPuzzle
        End Sub

    End Class
End Namespace
