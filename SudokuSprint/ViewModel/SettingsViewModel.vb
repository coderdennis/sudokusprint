﻿Imports System.IO.IsolatedStorage
Imports System.Xml.Linq

Namespace ViewModel
    Public Class SettingsViewModel
        Inherits PageViewModelBase

        Public Const DifficultyPropertyName As String = "Difficulty"
        Private ReadOnly _settings As IsolatedStorageSettings

        Public Sub New()
            _pageTitle = "settings"
            _settings = IsolatedStorageSettings.ApplicationSettings
            Load()
            '_difficulty = 55
        End Sub

        Private _difficulty As Integer
        ''' <summary>
        ''' Equals the number of locked squares. A higher number produces an easier puzzle.
        ''' </summary>
        Public Property Difficulty() As Double
            Get
                Return _difficulty
            End Get
            Set(ByVal value As Double)
                If Math.Abs(_difficulty - value) > 0.9 Then
                    _difficulty = value
                    OnPropertyChanged("Difficulty")
                End If
            End Set
        End Property

        Public Property State() As String
            Get
                Dim stateXml = <Settings>
                                   <Difficulty><%= _difficulty %></Difficulty>
                               </Settings>
                Return stateXml.ToString
            End Get
            Set(ByVal value As String)
                Dim stateXml = XElement.Parse(value)
                _difficulty = stateXml.<Difficulty>.Value
                OnPropertyChanged("")
            End Set
        End Property

        Public Function AddOrUpdateValue(key As String, value As Object) As Boolean
            Dim valueChanged As Boolean = False
            If _settings.Contains(Key) Then
                If Not _settings(Key).Equals(value) Then
                    _settings(Key) = value
                    valueChanged = True
                End If
            Else
                _settings.Add(Key, value)
                valueChanged = True
            End If
            Return valueChanged
        End Function

        Public Function GetValueOrDefault(key As String, defaultValue As Object) As Object
            Dim value As Object
            If _settings.Contains(Key) Then
                value = _settings(Key)
            Else
                value = defaultValue
            End If
            Return value
        End Function

        Private Sub Load()
            _difficulty = GetValueOrDefault(DifficultyPropertyName, 55)
        End Sub

        Public Sub Save()
            If AddOrUpdateValue(DifficultyPropertyName, _difficulty) Then
                _settings.Save()
            End If
        End Sub

    End Class
End Namespace