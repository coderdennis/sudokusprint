﻿Imports SudokuSprint.Helpers

Namespace ViewModel
    Public Class PageViewModelBase
        Inherits ViewModelBase

        Protected _pageTitle As String

        Public Sub New()
            _pageTitle = "welcome"
        End Sub

        Public ReadOnly Property ApplicationTitle As String
            Get
                Return AppManifestSettings.ApplicationTitle.ToUpper()
            End Get
        End Property

        Public ReadOnly Property PageTitle As String
            Get
                Return _pageTitle
            End Get
        End Property

    End Class
End Namespace