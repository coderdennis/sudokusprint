Imports System.ComponentModel

Namespace ViewModel

    ''' <summary>
    ''' Base class for all ViewModel classes in the application.
    ''' It provides support for property change notifications 
    ''' and has a DisplayName property.  This class is abstract.
    ''' </summary>
    Public MustInherit Class ViewModelBase
        Implements INotifyPropertyChanged

        ''' <summary>
        ''' Tell this ViewModel to raise it's PropertyChanged event.
        ''' </summary>
        Public Sub Refresh()
            OnPropertyChanged(Nothing)
        End Sub

        Public ReadOnly Property IsDesignMode As Boolean
            Get
                Return DesignerProperties.IsInDesignTool
            End Get
        End Property

        ''' <summary>
        ''' Raised when a property on this object has a new value.
        ''' </summary>
        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Raises this object's PropertyChanged event.
        ''' </summary>
        ''' <param name="propertyName">The property that has a new value.</param>
        Protected Sub OnPropertyChanged(ByVal propertyName As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub

    End Class

End Namespace