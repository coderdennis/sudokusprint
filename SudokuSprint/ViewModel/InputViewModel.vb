﻿Imports Sudoku.Core
Imports SudokuSprint.Helpers
Imports SudokuSprint.Commanding

Namespace ViewModel
    Public Class InputViewModel
        Inherits ViewModelBase

        Private ReadOnly _parent As PuzzleViewModel
        Public Sub New(ByVal parent As PuzzleViewModel)
            _parent = parent
            _inputVisible = False
            _left = 122
            _top = 103
            _square = New Square()
        End Sub

        Private _displayNotes As Boolean
        Public Property DisplayNotes() As Boolean
            Get
                Return _displayNotes
            End Get
            Set(ByVal value As Boolean)
                _displayNotes = value
                OnPropertyChanged("DisplayNotesVisibility")
                OnPropertyChanged("DisplayValuesVisibility")
            End Set
        End Property

        Public Sub UpdatePuzzle()
            _parent.UpdateImage()
        End Sub

        Public ReadOnly Property DisplayNotesVisibility As Visibility
            Get
                Return _displayNotes.AsVisibility
            End Get
        End Property

        Public ReadOnly Property DisplayValuesVisibility As Visibility
            Get
                Return (Not _displayNotes).AsVisibility
            End Get
        End Property

        Private _inputVisible As Boolean
        Public Property InputVisible() As Boolean
            Get
                Return _inputVisible
            End Get
            Set(ByVal value As Boolean)
                If _inputVisible <> value Then
                    _inputVisible = value
                    OnPropertyChanged("InputVisibility")
                End If
            End Set
        End Property

        Public ReadOnly Property InputVisibility As Visibility
            Get
                Return _inputVisible.AsVisibility
            End Get
        End Property

        Private _left As Double
        Public Property Left() As Double
            Get
                Return _left
            End Get
            Set(ByVal value As Double)
                If Math.Abs(_left - value) > 0.001 Then
                    _left = value
                    OnPropertyChanged("Left")
                End If
            End Set
        End Property

        Private _top As Double
        Public Property Top() As Double
            Get
                Return _top
            End Get
            Set(ByVal value As Double)
                If Math.Abs(_top - value) > 0.001 Then
                    _top = value
                    OnPropertyChanged("Top")
                End If
            End Set
        End Property

        Private _square As Square
        Public Property Square() As Square
            Get
                Return _square
            End Get
            Set(ByVal value As Square)
                _square = value
                OnPropertyChanged("")
            End Set
        End Property

        Private _setValueCommand As RelayCommand
        Public ReadOnly Property SetValueCommand As ICommand
            Get
                If _setValueCommand Is Nothing Then
                    _setValueCommand = New RelayCommand(AddressOf SetValue)
                End If
                Return _setValueCommand
            End Get
        End Property

        Private Sub SetValue(ByVal value As Values)
            If _square.Value = value Then
                _square.Value = Values.Empty
            Else
                _square.Value = value
            End If
            'InputVisible = False 'this is set by Tap event handler!
        End Sub

        Private _setNoteCommand As RelayCommand
        Public ReadOnly Property SetNoteCommand As ICommand
            Get
                If _setNoteCommand Is Nothing Then
                    _setNoteCommand = New RelayCommand(AddressOf SetNote)
                End If
                Return _setNoteCommand
            End Get
        End Property

        Public Sub SetNote(ByVal value As Values)
            If _square.PossibleValues.Contains(value) Then
                _square.PossibleValues.ManuallyRemove(value)
            Else
                _square.PossibleValues.ManuallyAdd(value)
            End If
            RaiseEvent NotesChanged(Me, EventArgs.Empty)
            OnPropertyChanged("")
        End Sub

        Public Event NotesChanged As EventHandler

        Public ReadOnly Property ValueOneBackground As Brush
            Get
                Return (_square.Value = Values.One).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueTwoBackground As Brush
            Get
                Return (_square.Value = Values.Two).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueThreeBackground As Brush
            Get
                Return (_square.Value = Values.Three).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueFourBackground As Brush
            Get
                Return (_square.Value = Values.Four).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueFiveBackground As Brush
            Get
                Return (_square.Value = Values.Five).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueSixBackground As Brush
            Get
                Return (_square.Value = Values.Six).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueSevenBackground As Brush
            Get
                Return (_square.Value = Values.Seven).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueEightBackground As Brush
            Get
                Return (_square.Value = Values.Eight).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property ValueNineBackground As Brush
            Get
                Return (_square.Value = Values.Nine).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteOneBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.One)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteTwoBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Two)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteThreeBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Three)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteFourBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Four)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteFiveBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Five)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteSixBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Six)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteSevenBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Seven)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteEightBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Eight)).ButtonBackground
            End Get
        End Property

        Public ReadOnly Property NoteNineBackground As Brush
            Get
                Return (_square.Value = Values.Empty AndAlso _square.PossibleValues.Contains(Values.Nine)).ButtonBackground
            End Get
        End Property
    End Class
End Namespace