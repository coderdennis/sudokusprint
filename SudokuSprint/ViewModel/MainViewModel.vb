﻿Imports System.Xml.Linq
Imports SudokuSprint.Helpers
#If Not DEBUG Then
Imports Microsoft.Phone.Marketplace
#End If
Imports PuzzleSource
Imports Sudoku.Core.Extensions
Imports System.Windows.Threading

Namespace ViewModel
    Public Class MainViewModel
        Inherits PageViewModelBase

        Private ReadOnly _puzzleViewModel As PuzzleViewModel
        Private _gameTime As TimeSpan
        Private _gameTimeDisplay As String
        Private _ticks As Long
        Private WithEvents _timer As DispatcherTimer

#If Not DEBUG Then
        Private Shared ReadOnly LicenseInfo As LicenseInformation = New LicenseInformation
#End If
        Private Shared _isTrial As Boolean

        Public ReadOnly Property IsTrial As Boolean
            Get
                Return _isTrial
            End Get
        End Property

        Public ReadOnly Property AdsVisibility As Visibility
            Get
                Return _isTrial.AsVisibility
            End Get
        End Property

        ''' <summary>
        ''' Check the current license information for this application
        ''' </summary>
        Private Sub CheckLicense()
            If AppManifestSettings.ApplicationTitle.Contains("Free") Then
                _isTrial = True
            Else
#If DEBUG Then
                Const message As String = "Press 'OK' to simulate trial mode. Press 'Cancel' to run the application in normal mode."
                    _isTrial = (MessageBox.Show(message, "Debug Trial", MessageBoxButton.OKCancel) = MessageBoxResult.OK)
#Else
            _isTrial = LicenseInfo.IsTrial()
#End If
            End If

            OnPropertyChanged("IsTrial")
            OnPropertyChanged("AdsVisibility")
        End Sub

        Public Sub New(ByVal puzzleSource As IPuzzleSource)
            _pageTitle = "puzzle"
            _gameTime = New TimeSpan
            _gameTimeDisplay = "00:00.00"
            _puzzleViewModel = New PuzzleViewModel(puzzleSource)
            _timer = New DispatcherTimer
            _timer.Interval = TimeSpan.FromMilliseconds(10)
            _ticks = 0
            _isTrial = True
        End Sub

        Public Property GameTime() As String
            Get
                Return _gameTimeDisplay
            End Get
            Private Set(value As String)
                If _gameTimeDisplay <> value Then
                    _gameTimeDisplay = value
                    OnPropertyChanged("GameTime")
                End If
            End Set
        End Property

        Private _puzzleIsSolvedStopOnNextTick As Boolean
        Private Sub Timer_Tick(ByVal sender As Object, ByVal e As EventArgs) Handles _timer.Tick
            If _puzzleIsSolvedStopOnNextTick Then
                _timer.Stop()
                MessageBox.Show("You solved the puzzle." & vbCrLf &
                                "Your time was " & GameTime, "Congratulations!", MessageBoxButton.OK)
            ElseIf _puzzleViewModel.IsSolved Then
                _puzzleIsSolvedStopOnNextTick = True
            Else
                Dim ticksNow = DateTime.Now.Ticks
                Dim elapsedTicks = ticksNow - _ticks
                _ticks = ticksNow
                _gameTime = _gameTime.Add(TimeSpan.FromTicks(elapsedTicks))
                GameTime = _gameTime.ToString("mm\:ss\.ff")
            End If
        End Sub

        Public Sub StartTimer()
            _puzzleIsSolvedStopOnNextTick = False
            _ticks = DateTime.Now.Ticks
            _timer.Start()
            Dim phoneAppService = PhoneApplicationService.Current
            phoneAppService.UserIdleDetectionMode = IdleDetectionMode.Disabled
            CheckLicense()
        End Sub

        Public Sub StopTimer()
            _timer.Stop()
            Dim phoneAppService = PhoneApplicationService.Current
            phoneAppService.UserIdleDetectionMode = IdleDetectionMode.Enabled
        End Sub

        Public ReadOnly Property PuzzleViewModel As PuzzleViewModel
            Get
                Return _puzzleViewModel
            End Get
        End Property

        Public Property State As String
            Get
                If _puzzleViewModel.IsSolved Then
                    Return ""
                End If
                'Return _puzzleViewModel.Puzzle.GetState.ToString
                Dim result = <gameState>
                                 <time><%= _gameTime.Ticks %></time>
                                 <displayNotes><%= _puzzleViewModel.InputContext.DisplayNotes.ToString.ToLower %></displayNotes>
                                 <%= _puzzleViewModel.Puzzle.GetState() %>
                             </gameState>
                Return result.ToString
            End Get
            Set(value As String)
                If Not String.IsNullOrEmpty(value) Then
                    Dim xState = XElement.Parse(value)
                    _gameTime = TimeSpan.FromTicks(Long.Parse(xState.<time>.Value))
                    GameTime = _gameTime.ToString("mm\:ss\.f")
                    _puzzleViewModel.InputContext.DisplayNotes = xState.<displayNotes>.Value = "true"

                    _puzzleViewModel.Puzzle.SetState(xState.<puzzle>.Single)
                    _puzzleViewModel.UpdateImage()
                Else
                    NewPuzzle()
                End If
            End Set
        End Property

        Public Sub ResetPuzzle()
            _puzzleViewModel.ResetPuzzle()
            StartTimer()
        End Sub

        Public Sub NewPuzzle()
            _puzzleViewModel.NewPuzzle()
            _gameTime = TimeSpan.FromMilliseconds(0)
            StartTimer()
        End Sub

        Sub ShowInput(point As Point)
            _puzzleViewModel.ShowInput(point)
        End Sub

    End Class

End Namespace