﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Resources

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("PuzzleSource")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyConfiguration("")> 
<Assembly: AssemblyCompany("Dennis Palmer")> 
<Assembly: AssemblyProduct("Sudoku Sprint")> 
<Assembly: AssemblyCopyright("Copyright © Dennis Palmer 2012")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: AssemblyCulture("")> 

' Setting ComVisible to false makes the types in this assembly not visible 
' to COM components.  If you need to access a type in this assembly from 
' COM, set the ComVisible attribute to true on that type.
<assembly: ComVisible(false)>

' The following GUID is for the ID of the typelib if this project is exposed to COM
<assembly: Guid("1305f9bf-9828-4bda-94c8-76fad1a64f43")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Revision and Build Numbers 
' by using the '*' as shown below:
<assembly: AssemblyVersion("1.0.0.0")>
<assembly: AssemblyFileVersion("1.0.0.0")>
<assembly: NeutralResourcesLanguageAttribute("en-US")>
