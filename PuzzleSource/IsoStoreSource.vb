﻿Imports System.ComponentModel
Imports System.IO.IsolatedStorage
Imports System.IO
Imports Sudoku.Core
Imports Sudoku.Core.Extensions

Public Class IsoStoreSource
    Implements IPuzzleSource

    Private Const PuzzlesToStore = 100
    Private Const FolderName = "PuzzleSource"
    Private Const FileName = "puzzles.txt"

    Private ReadOnly _puzzles As List(Of String)
    Private WithEvents _worker As BackgroundWorker

    Public Sub New()
        _puzzles = New List(Of String)
        LoadList()
        _worker = New BackgroundWorker
        _worker.RunWorkerAsync()
    End Sub

    Public Function GetPuzzle() As Puzzle Implements IPuzzleSource.GetPuzzle
        If _puzzles.Any Then
            Dim puzzle = New Puzzle
            puzzle.SuspendSquareValueChangedEvent = True
            puzzle.FromString(_puzzles.First)
            puzzle.SuspendSquareValueChangedEvent = False
            _puzzles.RemoveAt(0)
            If Not _worker.IsBusy Then
                _worker.RunWorkerAsync()
            End If
            Return puzzle
        Else
            Return GeneratorSource.GeneratePuzzle
        End If
    End Function

    Public Sub SaveState() Implements IPuzzleSource.SaveState
        SaveList()
    End Sub

    Private Sub Worker_DoWork(sender As Object, e As DoWorkEventArgs) Handles _worker.DoWork
        e.Result = GeneratorSource.GeneratePuzzle
    End Sub

    Private Sub Worker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles _worker.RunWorkerCompleted
        _puzzles.Add(e.Result.ToString)
        If Not _worker.IsBusy AndAlso _puzzles.Count < PuzzlesToStore Then
            _worker.RunWorkerAsync()
        End If
    End Sub

    Private Sub LoadList()
        Dim myStore = IsolatedStorageFile.GetUserStoreForApplication
        If myStore.FileExists(FolderName & "\" & FileName) Then
            Using isoFileStream = New IsolatedStorageFileStream(FolderName & "\" & FileName, FileMode.Open, myStore)
                Using isoFileReader = New StreamReader(isoFileStream)
                    While Not isoFileReader.EndOfStream
                        _puzzles.Add(isoFileReader.ReadLine)
                    End While
                End Using
            End Using
        Else
            Dim textFileUri = New Uri(FileName, UriKind.Relative)
            Dim textFileStream = Application.GetResourceStream(textFileUri)
            Using textFileReader = New StreamReader(textFileStream.Stream)
                Dim puzzles = New List(Of String)
                While Not textFileReader.EndOfStream
                    puzzles.Add(textFileReader.ReadLine)
                End While
                _puzzles.AddRange(puzzles.RandomSample(10, False)) '(puzzles.Take(10))
            End Using
        End If
    End Sub

    Private Sub SaveList()
        Dim myStore = IsolatedStorageFile.GetUserStoreForApplication

        If Not myStore.DirectoryExists(FolderName) Then
            myStore.CreateDirectory(FolderName)
        End If
        Using isoFileStream = New IsolatedStorageFileStream(FolderName & "\" & FileName, FileMode.Create, myStore)
            Using isoFileWriter = New StreamWriter(isoFileStream)
                For Each puzzleString In _puzzles
                    isoFileWriter.WriteLine(puzzleString)
                Next
            End Using
        End Using
    End Sub

End Class