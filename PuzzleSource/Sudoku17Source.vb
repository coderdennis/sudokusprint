﻿Imports System.IO
Imports Sudoku.Core
Imports Sudoku.Core.Extensions

Public Class Sudoku17Source
    Implements IPuzzleSource

    Private Const Filename = "sudoku17z.txt"
    Private ReadOnly _puzzles As List(Of String)

    Public Sub New()
        _puzzles = New List(Of String)
    End Sub

    Public Function GetPuzzle() As Puzzle Implements IPuzzleSource.GetPuzzle
        If Not _puzzles.Any Then
            Dim textFileUri = New Uri(Filename, UriKind.Relative)
            Dim textFileStream = Application.GetResourceStream(textFileUri)
            Using textFileReader = New StreamReader(textFileStream.Stream)
                While Not textFileReader.EndOfStream
                    _puzzles.Add(textFileReader.ReadLine)
                End While
            End Using
        End If
        Dim puzzle = New Puzzle
        puzzle.SuspendSquareValueChangedEvent = True
        puzzle.FromString(_puzzles.RandomSample(1, False).First)
        puzzle.SuspendSquareValueChangedEvent = False
        Return puzzle
    End Function

    Public Sub SaveState() Implements IPuzzleSource.SaveState
        'Nothing to do here.
    End Sub
End Class
