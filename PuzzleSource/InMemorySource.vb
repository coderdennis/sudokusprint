﻿Imports System.ComponentModel
Imports Sudoku.Core

'Worse performance than simple GeneratorSource!
' BackgroundWorker thread is slow.
Public Class InMemorySource
    Implements IPuzzleSource

    Private WithEvents _worker As BackgroundWorker
    Private _puzzle As Puzzle
    
    Public Sub New()
        _puzzle = Nothing
        _worker = New BackgroundWorker()
        _worker.RunWorkerAsync()
    End Sub

    Public Function GetPuzzle() As Sudoku.Core.Puzzle Implements IPuzzleSource.GetPuzzle
        If _puzzle IsNot Nothing Then
            Dim result = _puzzle
            _puzzle = Nothing
            _worker.RunWorkerAsync()
            Return result
        Else
            Return GeneratorSource.GeneratePuzzle
        End If
    End Function

    Public Sub SaveState() Implements IPuzzleSource.SaveState
        'Nothing to do here.
    End Sub

    Private Sub Worker_DoWork(sender As Object, e As DoWorkEventArgs) Handles _worker.DoWork
        e.Result = GeneratorSource.GeneratePuzzle
    End Sub

    Private Sub Worker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles _worker.RunWorkerCompleted
        _puzzle = e.Result
    End Sub

End Class
