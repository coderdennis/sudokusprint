﻿Imports Sudoku.Core

Public Class SampleSource
    Implements IPuzzleSource

    Public Function GetPuzzle() As Puzzle Implements IPuzzleSource.GetPuzzle
        Dim p = New Puzzle
        p.FromString("382570014659412837174398526416839752835267491297145368721954683548623179963781245")
        Return p
    End Function

    Public Sub SaveState() Implements IPuzzleSource.SaveState
        'nothing to do here
    End Sub
End Class
