﻿Imports Sudoku.Core

Public Interface IPuzzleSource

    Function GetPuzzle() As Puzzle
    Sub SaveState()
    
End Interface
