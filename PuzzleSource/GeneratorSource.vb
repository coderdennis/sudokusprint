﻿Imports Sudoku.Core

Public Class GeneratorSource
    Implements IPuzzleSource

    Public Function GetPuzzle() As Puzzle Implements IPuzzleSource.GetPuzzle
        Return GeneratePuzzle()
    End Function

    Public Shared Function GeneratePuzzle() As Puzzle
        Dim puzzle = New Puzzle
        Dim solver = New Solver
        solver.Solve(puzzle)
        Return puzzle
    End Function

    Public Sub SaveState() Implements IPuzzleSource.SaveState
        'Nothing to do here.
    End Sub

End Class
