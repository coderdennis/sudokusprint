﻿Imports NUnit.Framework
Imports ApprovalTests.Reporters
Imports ApprovalTests
Imports System.Text

<TestFixture()>
<UseReporter(GetType(DiffReporter))>
Public Class PuzzleTest

    <Test()>
    Public Sub Rows_HorizontalPuzzle_ReturnsCollectionOfRows()
        Dim testPuzzle = GetHorizontalPuzzle()
        Approvals.Approve(DisplayString(testPuzzle.Rows))
    End Sub

    <Test()>
    Public Sub Rows_VerticlePuzzle_ReturnsCollectionOfRows()
        Dim testPuzzle = GetVerticalPuzzle()
        Approvals.Approve(DisplayString(testPuzzle.Rows))
    End Sub


    <Test()>
    Public Sub Cols_HorizontalPuzzle_ReturnsCollectionOfColumns()
        Dim testPuzzle = GetHorizontalPuzzle()
        Approvals.Approve(DisplayString(testPuzzle.Cols))
    End Sub

    <Test()>
    Public Sub Cols_VerticlePuzzle_ReturnsCollectionOfCols()
        Dim testPuzzle = GetVerticalPuzzle()
        Approvals.Approve(DisplayString(testPuzzle.Cols))
    End Sub

    <Test()>
    Public Sub Blocks_HorizontalPuzzle_ReturnsCollectionOfBlocks()
        Dim testPuzzle = GetHorizontalPuzzle()
        Approvals.Approve(DisplayString(testPuzzle.Blocks))
    End Sub

    <Test()>
    Public Sub Blocks_VerticlePuzzle_ReturnsCollectionOfBlocks()
        Dim testPuzzle = GetVerticalPuzzle()
        Approvals.Approve(DisplayString(testPuzzle.Blocks))
    End Sub

    <Test()>
    Public Sub Blocks_NewPuzzle_ReturnsCollectionOfBlocks()
        Dim testPuzzle = New Puzzle
        Dim blocks = testPuzzle.Blocks
        Assert.AreEqual(9, blocks.Count, "There should be 9 blocks.")
        Assert.AreSame(testPuzzle.Square(0), blocks.First.First, "First square in first block should be the first square")
        Assert.AreSame(testPuzzle.Square(20), blocks.First.Last, "Last square in first block should be 21st square")
        Assert.AreSame(testPuzzle.Square(3), blocks(3).First)
        Assert.AreSame(testPuzzle.Square(23), blocks(3).Last)
        Assert.AreSame(testPuzzle.Square(6), blocks(6).First)
        Assert.AreSame(testPuzzle.Square(26), blocks(6).Last)
        Assert.AreSame(testPuzzle.Square(27), blocks(1).First)
        Assert.AreSame(testPuzzle.Square(47), blocks(1).Last)
        Assert.AreSame(testPuzzle.Square(30), blocks(4).First)
        Assert.AreSame(testPuzzle.Square(50), blocks(4).Last)
        Assert.AreSame(testPuzzle.Square(33), blocks(7).First)
        Assert.AreSame(testPuzzle.Square(53), blocks(7).Last)
        Assert.AreSame(testPuzzle.Square(54), blocks(2).First)
        Assert.AreSame(testPuzzle.Square(74), blocks(2).Last)
        Assert.AreSame(testPuzzle.Square(57), blocks(5).First)
        Assert.AreSame(testPuzzle.Square(77), blocks(5).Last)
        Assert.AreSame(testPuzzle.Square(60), blocks.Last.First)
        Assert.AreSame(testPuzzle.Square(80), blocks.Last.Last, "Last square in last block should be the last square")
    End Sub

    <Test()>
    Public Sub RowFromSquare_CenterSquare_ReturnsRowContainingSquare()
        Dim testPuzzle = New Puzzle
        Dim row = testPuzzle.RowFromSquare(testPuzzle.Square(40))
        Assert.Contains(testPuzzle.Square(40), row)
        Assert.AreSame(testPuzzle.Square(40), row(4))
    End Sub

    <Test()>
    <ExpectedException(ExpectedExceptionName:="System.InvalidOperationException", ExpectedMessage:="Sequence contains no elements")>
    Public Sub RowFromSquare_NewSquare_Throws()
        Dim testPuzzle = New Puzzle
        Dim testSquare = New Square()
        testPuzzle.RowFromSquare(testSquare)
    End Sub

    <Test()>
    Public Sub FromString_ValidPuzzle_IsSolved()
        Dim testPuzzle = New Puzzle
        testPuzzle.FromString("382576914659412837174398526416839752835267491297145368721954683548623179963781245")
        Assert.IsTrue(testPuzzle.IsSolved)
    End Sub

    <Test()>
    Public Sub FromString_ValidPuzzleWithLocks_ProperSquaresAreLocked()
        Dim testPuzzle = New Puzzle
        testPuzzle.FromString("382576914659412837174398526416839752835267491297145368721954683548623179963781245:" &
                              "010101010101010101010101010101010101010101010101010101010101010101010101010101010")
        testPuzzle.ClearUnlockedSquares()
        Approvals.Approve(DisplayString(testPuzzle.Rows))
    End Sub

    Private Function GetHorizontalPuzzle() As Puzzle
        Dim result = New Puzzle
        For i = 0 To 8
            For j = 0 To 8
                Dim s = (i * 9) + j
                result.SquareValue(s) = i + 1
            Next
        Next
        Return result
    End Function

    Private Function GetVerticalPuzzle() As Puzzle
        Dim result = New Puzzle
        For i = 0 To 8
            For j = 0 To 8
                Dim s = (i * 9) + j
                result.SquareValue(s) = j + 1
            Next
        Next
        Return result
    End Function

    Private Function DisplayString(ByVal list As List(Of List(Of Square))) As String
        Dim sb = New StringBuilder
        For Each x In list
            For Each y In x
                sb.Append(y.Value)
            Next
            sb.AppendLine()
        Next
        Return sb.ToString
    End Function

End Class
