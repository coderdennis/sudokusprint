﻿Imports NUnit.Framework
Imports Sudoku.Core.Extensions

<TestFixture()>
Public Class SolverTest

    <Test()>
    Public Sub Solve_BlankPuzzle_GeneratesSolvedPuzzle()
        Dim puzzle = New Puzzle
        Dim solver = New Solver
        solver.Solve(puzzle)
        Assert.IsTrue(puzzle.IsSolved)
    End Sub

    <Test()>
    Public Sub Solve_PartiallyLockedPuzzle_DoesNotAttemptToModifyLockedSquares()
        Dim puzzle = New Puzzle
        puzzle.FromString("382576914659412837174398526416839752835267491297145368721954683548623179963781245")
        ' lock 20 squares and clear others
        For Each sq In puzzle.Squares.RandomSample(20, 500, False)
            sq.Locked = True
        Next
        puzzle.ClearUnlockedSquares()

        Dim solver = New Solver()
        solver.Solve(puzzle)
        Assert.IsTrue(puzzle.IsSolved)
    End Sub

End Class
