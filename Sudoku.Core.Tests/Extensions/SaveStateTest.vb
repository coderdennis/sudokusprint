﻿Imports NUnit.Framework
Imports ApprovalTests.Reporters
Imports Sudoku.Core.Extensions
Imports ApprovalTests

Namespace Extensions

    <TestFixture()>
    <UseReporter(GetType(DiffReporter))>
    Public Class SaveStateTest

        <Test()>
        Public Sub ApproveLockedSquareState()
            Dim sq = New Square
            sq.Value = Values.Five
            sq.Locked = True
            Dim squareState As String = sq.GetState.ToString & vbCrLf
            Approvals.Approve(squareState)
        End Sub

        <Test()>
        Public Sub ApprovePossibleValuesState()
            Dim values = New PossibleValues
            values.ManuallyRemove(Core.Values.Five)
            values.Add(Core.Values.Five)
            Approvals.Approve(values.GetState.ToString & vbCrLf)
        End Sub

        <Test()>
        Public Sub ApprovePuzzleState()
            Dim puzzle = SamplePuzzle.GetSamplePuzzle(30, 15)
            Approvals.Approve(puzzle.GetState.ToString & vbCrLf)
        End Sub

        <Test()>
        Public Sub Square_ValueFive_IsSet()
            Dim sq = New Square
            sq.Value = Values.Five
            Dim state = sq.GetState
            Dim sq2 = New Square
            sq2.SetState(state)
            Assert.AreEqual(Values.Five, sq2.Value)
        End Sub

    End Class
End Namespace