﻿Imports NUnit.Framework

<TestFixture()>
Public Class PossibleValuesTest

    Private _testValues As PossibleValues

    <SetUp()>
    Public Sub SetUp()
        _testValues = New PossibleValues
    End Sub

    <TearDown()>
    Public Sub TearDown()
        _testValues = Nothing
    End Sub

    <Test()>
    Public Sub New_ContainsValuesOneToNine()
        Assert.AreEqual(New List(Of Values) From {1, 2, 3, 4, 5, 6, 7, 8, 9}, _testValues)
    End Sub

    <Test()>
    Public Sub ManuallyRemovedValue_DoesNotGetAdded()
        _testValues.ManuallyRemove(Values.Five)
        Assert.AreEqual(New List(Of Values) From {1, 2, 3, 4, 6, 7, 8, 9}, _testValues, "Value Five should have been removed.")
        _testValues.Add(Values.Five)
        Assert.AreEqual(New List(Of Values) From {1, 2, 3, 4, 6, 7, 8, 9}, _testValues, "Value Five should not have been added.")
    End Sub

    <Test()>
    Public Sub ManuallyRemovedAndAddedValue_DoesGetAdded()
        _testValues.ManuallyRemove(Values.Five)
        Assert.AreEqual(New List(Of Values) From {1, 2, 3, 4, 6, 7, 8, 9}, _testValues, "Value Five should have been removed.")
        _testValues.ManuallyAdd(Values.Five)
        Assert.AreEqual(New List(Of Values) From {1, 2, 3, 4, 6, 7, 8, 9, 5}, _testValues, "Value Five should have been added.")
    End Sub

    <Test()>
    Public Sub FromEnumerable_DoesNotAddManuallyRemovedValues()
        _testValues.ManuallyRemove(Values.Five)
        _testValues.ManuallyRemove(Values.One)
        _testValues.FromEnumerable(New List(Of Values) From {1, 3, 5, 7, 9})
        Assert.AreEqual(New List(Of Values) From {3, 7, 9}, _testValues)
    End Sub

End Class
