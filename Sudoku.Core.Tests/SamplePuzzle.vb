﻿Imports Sudoku.Core.Extensions

Public Class SamplePuzzle

    Private Shared _puzzle As Puzzle

    Public Shared Function GetSamplePuzzle(ByVal lockedSquares As Integer, ByVal filledSquares As Integer) As Puzzle
        If _puzzle Is Nothing Then
            _puzzle = New Puzzle
            _puzzle.SuspendSquareValueChangedEvent = True
            _puzzle.FromString("382576914659412837174398526416839752835267491297145368721954683548623179963781245")
            For Each sq In _puzzle.Squares.RandomSample(lockedSquares, 500, False)
                sq.Locked = True
            Next
            For Each sq In _puzzle.UnlockedSquares.RandomSample(81 - lockedSquares - filledSquares, 500, False)
                sq.Value = Values.Empty
            Next
            _puzzle.SuspendSquareValueChangedEvent = False
            _puzzle.ResetAllPossibleValues()
        End If
        Return _puzzle
    End Function

End Class
