﻿Public Class Square

    Public Property Locked As Boolean

    Private _value As Values
    Public Property Value As Values
        Get
            Return _value
        End Get
        Set(ByVal val As Values)
            If Locked Then
                Throw New InvalidOperationException("Square value cannot be changed because it is locked.")
            End If
            If _value <> val Then
                Dim oldValue = _value
                _value = val
                RaiseEvent ValueChanged(Me, New SquareValueChangedEventArgs(oldValue, val))
            End If
        End Set
    End Property

    Private ReadOnly _possibleValues As PossibleValues
    Public ReadOnly Property PossibleValues As PossibleValues
        Get
            Return _possibleValues
        End Get
    End Property

    Public Sub New()
        _value = Values.Empty
        _possibleValues = New PossibleValues
    End Sub

    Public Delegate Sub SquareValueChangedHandler(ByVal sender As Object, ByVal e As SquareValueChangedEventArgs)

    Public Event ValueChanged As SquareValueChangedHandler

End Class

Public Class SquareValueChangedEventArgs
    Inherits EventArgs

    Public Property OldValue As Values

    Public Property NewValue As Values

    Public Sub New(ByVal oldValue As Values, ByVal newValue As Values)
        Me.OldValue = oldValue
        Me.NewValue = newValue
    End Sub

End Class
