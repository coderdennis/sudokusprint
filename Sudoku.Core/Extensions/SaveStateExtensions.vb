﻿Imports System.Runtime.CompilerServices
Imports System.Xml.Linq

Namespace Extensions
    Public Module SaveStateExtensions

        <Extension()>
        Public Function GetState(ByVal puzzle As Puzzle) As XElement
            Dim result = <puzzle>
                             <%= From sq In puzzle.Squares
                                 Select GetState(sq)
                             %>
                         </puzzle>
            Return result
        End Function

        <Extension()>
        Public Sub SetState(ByVal puzzle As Puzzle, state As XElement)
            puzzle.SuspendSquareValueChangedEvent = True
            Dim squareStates = state.<square>.ToList
            For i = 0 To 80
                puzzle.Square(i).SetState(squareStates(i))
            Next
            puzzle.SuspendSquareValueChangedEvent = False
            puzzle.ResetAllPossibleValues()
        End Sub

        <Extension()>
        Public Function GetState(ByVal square As Square) As XElement
            Dim result = <square isLocked=<%= square.Locked %> value=<%= square.Value %>/>
            If Not square.Locked Then
                result.Add(square.PossibleValues.GetState)
            End If
            Return result
        End Function

        <Extension()>
        Public Sub SetState(ByVal square As Square, state As XElement)
            square.Value = [Enum].Parse(GetType(Values), state.@value, True)
            square.Locked = state.@isLocked.AsBoolean
            If Not square.Locked Then
                square.PossibleValues.SetState(state.<possibleValues>.SingleOrDefault)
            End If
        End Sub

        <Extension()>
        Public Function AsBoolean(ByVal value As String) As Boolean
            Return (value.ToLower = "true" OrElse value = "1")
        End Function

        <Extension()>
        Public Function GetState(ByVal possibleValues As PossibleValues) As XElement
            Dim result = <possibleValues>
                             <%= From v In possibleValues._manuallyRemoved
                                 Select <remove><%= v %></remove>
                             %>
                             <%= From v In possibleValues
                                 Select <add><%= v %></add>
                             %>
                         </possibleValues>
            Return result
        End Function

        <Extension()>
        Public Sub SetState(ByVal possibleValues As PossibleValues, state As XElement)
            If state IsNot Nothing Then
                For Each v In state.<remove>
                    possibleValues.ManuallyRemove([Enum].Parse(GetType(Values), v.Value, True))
                Next
                For Each v In state.<add>
                    possibleValues.Add([Enum].Parse(GetType(Values), v.Value, True))
                Next
            End If
        End Sub

    End Module
End Namespace