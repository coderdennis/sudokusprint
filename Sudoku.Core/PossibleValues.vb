﻿Imports System.Collections.ObjectModel

Public Class PossibleValues
    Inherits ObservableCollection(Of Values)

    ' ReSharper disable InconsistentNaming
    Friend _manuallyRemoved As List(Of Values)
    ' ReSharper restore InconsistentNaming

    Public Sub New()
        For Each v In AllNonEmptyValues
            MyBase.Add(v)
        Next
        _manuallyRemoved = New List(Of Values)
    End Sub

    Public Sub ManuallyRemove(ByVal value As Values)
        Remove(value)
        If Not _manuallyRemoved.Contains(value) Then
            _manuallyRemoved.Add(value)
        End If
    End Sub

    Public Shadows Sub Add(ByVal value As Values)
        If Not Contains(value) AndAlso Not _manuallyRemoved.Contains(value) Then
            MyBase.Add(value)
        End If
    End Sub

    Public Sub ManuallyAdd(ByVal value As Values)
        _manuallyRemoved.Remove(value)
        Add(value)
    End Sub

    Public Sub FromEnumerable(ByVal list As IEnumerable(Of Values))
        Clear()
        For Each v In list
            Add(v)
        Next
    End Sub

    Public Sub ResetManualList()
        _manuallyRemoved.Clear()
    End Sub

End Class
