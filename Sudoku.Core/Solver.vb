﻿Imports Sudoku.Core.Extensions

Public Class Solver

    Private Shared Sub CheckGroupForUniquePossibleValue(ByVal group As List(Of Square))
        Dim valueDict = AllNonEmptyValues.ToDictionary(Function(v) v, Function(v) 0)
        For Each sq In group.Where(Function(s) s.Value = Values.Empty)
            For Each v In sq.PossibleValues
                valueDict(v) += 1
            Next
        Next
        Dim uniqueValues = From v In valueDict
                           Where v.Value = 1
                           Select v.Key

        For Each loopValue In uniqueValues
            Dim v = loopValue
            Dim sq = group.Where(Function(s) s.Value = Values.Empty AndAlso s.PossibleValues.Contains(v)).SingleOrDefault
            If sq IsNot Nothing Then
                sq.Value = v
            End If
        Next
    End Sub

    Public Function Solve(ByVal puzzle As Puzzle) As Boolean

        'Find all empty squares with only one possible value and fill them
        Dim singlePossibleValueSquares = (From square In puzzle.Squares
                                          Where (square.Value = Values.Empty) AndAlso
                                                (square.PossibleValues.Count = 1)).ToList

        While singlePossibleValueSquares.Any
            For Each sq In singlePossibleValueSquares
                If sq.PossibleValues.Count = 1 Then 'This may change within the loop if there is more than one square
                    sq.Value = sq.PossibleValues.Single
                Else
                    Return False
                End If
            Next

            'check for any empty squares with no possible values
            If (From square In puzzle.Squares
                Where square.Value = Values.Empty And square.PossibleValues.Count = 0).Any Then
                Return False
            End If

            'find any row, col or group with a square with a possible value that only occurs once within that group
            For Each row In puzzle.Rows
                CheckGroupForUniquePossibleValue(row)
            Next
            For Each col In puzzle.Cols
                CheckGroupForUniquePossibleValue(col)
            Next
            For Each block In puzzle.Blocks
                CheckGroupForUniquePossibleValue(block)
            Next
            
            singlePossibleValueSquares = (From square In puzzle.Squares
                                          Where (square.Value = Values.Empty) AndAlso
                                                (square.PossibleValues.Count = 1)).ToList
        End While
        
        If puzzle.IsSolved Then
            Return True
        Else

            'check for any empty squares with no possible values
            If (From square In puzzle.Squares
                Where square.Value = Values.Empty And square.PossibleValues.Count = 0).Any Then
                Return False
            End If

            'iterate over all remaining empty squares and try all possible values
            Dim remainingSquares = (From square In puzzle.Squares
                                   Where square.Value = Values.Empty
                                   Order By square.PossibleValues.Count).ToList

            For Each sq In remainingSquares
                For Each v In sq.PossibleValues
                    Dim i = puzzle.Squares.IndexOf(sq)
                    Dim newPuzzle = New Puzzle
                    newPuzzle.SetState(puzzle.GetState)
                    newPuzzle.Square(i).Value = v
                    If Solve(newPuzzle) Then
                        For x = 0 To 80
                            If Not puzzle.Square(x).Locked Then
                                puzzle.Square(x).Value = newPuzzle.Square(x).Value
                            End If
                        Next
                        Return True
                    End If
                Next
            Next

        End If
        Return False
    End Function

End Class