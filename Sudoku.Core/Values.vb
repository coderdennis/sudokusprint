﻿Public Enum Values As Byte
    Empty = 0
    One = 1
    Two = 2
    Three = 3
    Four = 4
    Five = 5
    Six = 6
    Seven = 7
    Eight = 8
    Nine = 9
End Enum

Public Module ValuesModule

    Public AllNonEmptyValues As List(Of Values) = New List(Of Values) From {1, 2, 3, 4, 5, 6, 7, 8, 9}

End Module