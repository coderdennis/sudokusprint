﻿Imports System.Text

Public Class Puzzle

    Private ReadOnly _squares As List(Of Square)
    Private ReadOnly _rows As List(Of List(Of Square))
    Private ReadOnly _cols As List(Of List(Of Square))
    Private ReadOnly _blks As List(Of List(Of Square))

    Public Property SuspendSquareValueChangedEvent As Boolean

    Public Sub New()
        _squares = New List(Of Square)
        For i = 1 To 81
            Dim sq = New Square()
            _SuspendSquareValueChangedEvent = False
            AddHandler sq.ValueChanged, AddressOf _squareValueChanged
            _squares.Add(sq)
        Next
        _rows = (From r In Enumerable.Range(0, 9)
                Select _squares.Skip(r * 9).Take(9).ToList).ToList
        Dim colOrder = From c In Enumerable.Range(0, 9)
                       From r In _rows
                       Select r(c)
        _cols = (From c In Enumerable.Range(0, 9)
                Select colOrder.Skip(c * 9).Take(9).ToList).ToList

        Dim blockRows = From br In Enumerable.Range(0, 27)
                        Select _squares.Skip(br * 3).Take(3)

        Dim blockOrder1 = New List(Of Square)
        Dim blockOrder2 = New List(Of Square)
        Dim blockOrder3 = New List(Of Square)

        For i = 0 To 26
            Select Case i Mod 3
                Case 0
                    blockOrder1.AddRange(blockRows(i))
                Case 1
                    blockOrder2.AddRange(blockRows(i))
                Case 2
                    blockOrder3.AddRange(blockRows(i))
            End Select
        Next

        Dim blockOrder = New List(Of Square)

        blockOrder.AddRange(blockOrder1)
        blockOrder.AddRange(blockOrder2)
        blockOrder.AddRange(blockOrder3)

        _blks = (From b In Enumerable.Range(0, 9)
                Select blockOrder.Skip(b * 9).Take(9).ToList).ToList
    End Sub

    Public Delegate Sub SquareValueChangedHandler(ByVal sender As Object, ByVal e As SquareValueChangedEventArgs)

    Public Event SquareValueChanged As SquareValueChangedHandler

    Public ReadOnly Property Square(ByVal index As Byte) As Square
        Get
            Return _squares(index)
        End Get
    End Property

    Public Property SquareValue(ByVal index As Byte) As Values
        Get
            Return _squares(index).Value
        End Get
        Set(ByVal value As Values)
            _squares(index).Value = value
        End Set
    End Property

    Public ReadOnly Property Squares As IList(Of Square)
        Get
            Return _squares
        End Get
    End Property

    Public ReadOnly Property Rows As List(Of List(Of Square))
        Get
            Return _rows
        End Get
    End Property

    Public ReadOnly Property Cols As List(Of List(Of Square))
        Get
            Return _cols
        End Get
    End Property

    Public ReadOnly Property Blocks As List(Of List(Of Square))
        Get
            Return _blks
        End Get
    End Property

    Public ReadOnly Property UnlockedSquares As IEnumerable(Of Square)
        Get
            Return _squares.Where(Function(s) Not s.Locked)
        End Get
    End Property

    Public Sub ClearUnlockedSquares()
        For Each sq In UnlockedSquares
            sq.Value = Values.Empty
        Next
    End Sub

    Private Shared Function GroupFromSquare(ByVal groups As List(Of List(Of Square)), ByVal square As Square) As List(Of Square)
        Return (From g In groups
                Where g.Contains(square)).Single
    End Function

    Public Function RowFromSquare(ByVal aSquare As Square) As List(Of Square)
        Return GroupFromSquare(_rows, aSquare)
    End Function

    Public Function ColFromSquare(ByVal aSquare As Square) As List(Of Square)
        Return GroupFromSquare(_cols, aSquare)
    End Function

    Public Function BlockFromSquare(ByVal aSquare As Square) As List(Of Square)
        Return GroupFromSquare(_blks, aSquare)
    End Function

    Private Sub _squareValueChanged(ByVal aSquare As Object, ByVal e As SquareValueChangedEventArgs)
        If Not _SuspendSquareValueChangedEvent Then
            If e.OldValue <> Values.Empty Then
                'reset possible values for affected squares
                For Each sq In RowFromSquare(aSquare)
                    ResetPossibleValuesForSquare(sq)
                Next
                For Each sq In ColFromSquare(aSquare)
                    If sq IsNot aSquare Then
                        ResetPossibleValuesForSquare(sq)
                    End If
                Next
                For Each sq In BlockFromSquare(aSquare)
                    If sq IsNot aSquare Then
                        ResetPossibleValuesForSquare(sq)
                    End If
                Next
            End If
            'Find row, col & block that this square belongs to and remove new value from possible values of all those squares
            If e.NewValue <> Values.Empty Then
                For Each sq In RowFromSquare(aSquare)
                    sq.PossibleValues.Remove(e.NewValue)
                Next
                For Each sq In ColFromSquare(aSquare)
                    sq.PossibleValues.Remove(e.NewValue)
                Next
                For Each sq In BlockFromSquare(aSquare)
                    sq.PossibleValues.Remove(e.NewValue)
                Next
            End If
            RaiseEvent SquareValueChanged(aSquare, e)
        End If
    End Sub

    Private Sub ResetPossibleValuesForSquare(ByVal aSquare As Square)
        Dim rowValues = From sq In RowFromSquare(aSquare)
                        Select sq.Value
        Dim colValues = From sq In ColFromSquare(aSquare)
                        Select sq.Value
        Dim blockValues = From sq In BlockFromSquare(aSquare)
                          Select sq.Value
        aSquare.PossibleValues.FromEnumerable(AllNonEmptyValues.Except(rowValues.Union(colValues).Union(blockValues)))
    End Sub

    Public Sub ResetAllPossibleValues()
        For i = 0 To 80
            ResetPossibleValuesForSquare(_squares(i))
        Next
    End Sub

    Private Shared Function GroupContainsOneThroughNine(ByVal group As List(Of Square)) As Boolean
        Dim sortedGroupValues = From sq In group
                                Select sq.Value
                                Order By Value

        Return sortedGroupValues.SequenceEqual(AllNonEmptyValues)
    End Function

    Public Function IsSolved() As Boolean
        If _squares.Any(Function(s) s.Value = Values.Empty) Then
            Return False
        End If
        Return _rows.All(Function(row) GroupContainsOneThroughNine(row)) AndAlso
               _cols.All(Function(col) GroupContainsOneThroughNine(col)) AndAlso
               _blks.All(Function(block) GroupContainsOneThroughNine(block))
    End Function

    Public Overrides Function ToString() As String
        Dim sb = New StringBuilder
        For Each sq In _squares
            sb.Append(CType(sq.Value, Byte))
        Next
        If _squares.Any(Function(s) s.Locked) Then
            sb.Append(":")
            For Each sq In _squares
                sb.Append(If(sq.Locked, "1", "0"))
            Next
        End If
        Return sb.ToString
    End Function

    Public Sub FromString(ByVal value As String)
        For Each sq In _squares
            sq.Locked = False
        Next
        Dim squareValues As String
        Dim lockedValues = String.Empty
        If value.Contains(":") Then
            Dim values = value.Split(":")
            squareValues = values(0)
            lockedValues = values(1)
        Else
            squareValues = value
        End If
        If squareValues.Length <> 81 Then
            Throw New ArgumentOutOfRangeException("value", "input string must be of length 81.")
        End If
        For i = 0 To 80
            _squares(i).Value = Byte.Parse(value(i))
        Next
        If Not String.IsNullOrEmpty(lockedValues) Then
            For i = 0 To 80
                _squares(i).Locked = (lockedValues(i) = "1")
            Next
        End If
    End Sub

    Public Sub LockAllNonEmptySquares()
        For Each sq In _squares.Where(Function(s) s.Value <> Values.Empty)
            sq.Locked = True
        Next
    End Sub

    Public Sub Reset()
        For Each sq In _squares
            sq.PossibleValues.ResetManualList()
        Next
        For Each sq In UnlockedSquares
            sq.Value = Values.Empty
        Next
    End Sub

End Class
